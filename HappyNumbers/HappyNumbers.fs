﻿module HappyNumbers

let digits num =
    let rec loop n acc =
        match System.Math.DivRem(abs n, 10) with
        | (0, r) -> r :: acc
        | (x, r) -> loop x (r :: acc)
    loop num []

let digitSum num = digits num |> List.reduce (+)

let square i = i * i

let digitSquareSum num = digits num |> List.map square |> List.reduce (+)

let happySeq num =
    let rec loop n acc =
        match digitSquareSum n with
        | 1 -> 1 :: acc
        | x ->
            if Seq.exists ((=) x) acc then
                acc
            else
                loop x (x :: acc)
    let happyNums = loop num [num]
    (List.head happyNums = 1, List.rev happyNums)
