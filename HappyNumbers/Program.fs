﻿open System
open HappyNumbers

[<EntryPoint>]
let main argv =
    printf "Enter a number: "
    let input = int <| Console.ReadLine()
    
    printfn "Digits: %A" <| digits input
    printfn "Digit Sum: %i" <| digitSum input
    printfn "Digit Square Sum: %i" <| digitSquareSum input
    
    let isHappy, happyNums = happySeq input
    printfn "Is Happy? %b" isHappy
    printfn "Happy Sequence: %A" <| happyNums

    printfn "Press Enter to exit"
    Console.ReadLine() |> ignore
    0
